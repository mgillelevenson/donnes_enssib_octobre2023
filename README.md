# Cours d'introduction au HTR -- ENSSIB -- 2 octobre 2023


Ce dépôt contient les données d'entraînement qui seront produites comme exercice du cours d'introduction à la transcription automatisée. 


Elles sont extraites du dépôt CREMMA-AN Testament de poilus: 

https://github.com/HTR-United/CREMMA-AN-TestamentDePoilus



